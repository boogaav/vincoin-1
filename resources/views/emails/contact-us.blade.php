@component('mail::message')
# New message from Vincoin

You received a message from : {{ $message->name }}

<p>
  Name: {{ $message->name }}
</p>

<p>
  Email: {{ $message->email }}
</p>

<p>
  Message: {{ $message->message }}
</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
