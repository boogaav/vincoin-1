@extends("layouts.main")

<?php
$color_pallete = [
    [ 'color_hex' => '#d32f2f > #f44336', 'bg-color' => 'red-gradient', 'text-color' => 'white' ],
    [ 'color_hex' => '#f44336', 'bg-color' => 'tomato', 'text-color' => 'white' ],
    [ 'color_hex' => '#d32f2f', 'bg-color' => 'reddish', 'text-color' => 'white' ],
    [ 'color_hex' => '#f3f1f1', 'bg-color' => 'white', 'text-color' => 'black' ],
    [ 'color_hex' => '#ffcdd2', 'bg-color' => 'light-rose', 'text-color' => 'black' ],
    [ 'color_hex' => '#ffef62', 'bg-color' => 'yellow', 'text-color' => 'black' ],
    [ 'color_hex' => '#1b1b1b', 'bg-color' => 'grey-darkest', 'text-color' => 'white' ],
    [ 'color_hex' => '#bdbdbd', 'bg-color' => 'grey', 'text-color' => 'white' ],
    [ 'color_hex' => '#777777', 'bg-color' => 'grey-darker', 'text-color' => 'white' ],
];
?>

@section("content")

    <div class="container mx-auto">
        <header class="bg-orange-light text-lg text-grey p-8 mb-8">HEADER</header>

        <div class="inline-flex flex-wrap px-4 mb-10">
            <div class="mr-12">
                <div class="inline-block min-w-180 font-sans text-lg text-black font-normal pb-2 pr-3 border-b-2 border-black mb-8">Desktop typography</div>

                <div class="mb-3">
                    <h1 class="heading heading--lg">H1 Header</h1>
                </div>
                <div class="mb-3">
                    <h2 class="heading heading--md">H2 Header</h2>
                </div>
                <div class="mb-3">
                    <h3 class="heading heading--sm">H3 Header</h3>
                </div>
                <div class="mb-3">
                    <h4 class="heading heading--xs">H4 Header</h4>
                </div>
                <div class="mb-3">
                    <p class="paragraph">Paragraph main text</p>
                </div>
                <div class="mb-3"><a class="nav__link" href="#">Menu text</a></div>
                <div class="mb-3">
                    <div class="info-text">Info text style 1</div>
                </div>
                <div class="info-text info-text--alt">Info text style 2</div>
            </div>
            <div class="mr-12">
                <div class="inline-block min-w-180 font-sans text-lg text-black font-normal pb-2 pr-3 border-b-2 border-black mb-8">Mobile typography</div>
                <div class="mb-3">
                    <h1 class="heading heading--lg heading--lg-mobile">H1 Header</h1>
                </div>
                <div class="mb-3">
                    <h2 class="heading heading--md heading--md-mobile">H2 Header</h2>
                </div>
                <div class="mb-3">
                    <h3 class="heading heading--sm heading--sm-mobile">H3 Header</h3>
                </div>
                <div class="mb-3">
                    <h4 class="heading heading--xs heading--xs-mobile">H4 Header</h4>
                </div>
                <div class="mb-3">
                    <p class="paragraph">Paragraph main text</p>
                </div>
                <div class="mb-3"><a class="nav__link" href="#">Menu text</a></div>
                <div class="mb-3">
                    <div class="info-text info-text-mobile">Info text style 1</div>
                </div>
                <div class="info-text info-text--alt info-text-mobile">Info text style 2</div>
            </div>
            <div class="mr-12">
                <div class="inline-block min-w-180 font-sans text-lg text-black font-normal pb-2 pr-3 border-b-2 border-black mb-8">Elements</div>

                <div class="mb-6">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Normal</div>
                    <button class="btn btn--red" type="button" disabled>Preview</button>
                </div>
                <div class="mb-6">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Hover</div>
                    <button class="btn btn--red btn--red-hovered" type="button">Preview</button>
                </div>
                <div class="mb-6">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Pressed</div>
                    <button class="btn btn--red btn--red-focused" type="button">Preview</button>
                </div>

                <div class="mb-6">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Normal</div>
                    <button class="btn btn--white" type="button">Preview</button>
                </div>
                <div class="mb-6">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Hover</div>
                    <button class="btn btn--white btn--white-hovered" type="button">Preview</button>
                </div>
                <div class="mb-6">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Pressed</div>
                    <button class="btn btn--white btn--white-focused" type="button">Preview</button>
                </div>

                <div class="mb-6 bg-tomato p-4">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Transparent</div>
                    <button class="btn btn--transparent mx-auto" type="button">Preview</button>
                </div>
                <div class="mb-6 bg-tomato p-4">
                    <div class="mb-1 font-medium text-sm text-center text-black tracking-wide">Transparent hover</div>
                    <button class="btn btn--transparent mx-auto btn--transparent-hovered" type="button">Preview</button>
                </div>
            </div>
            <div>
                <div class="inline-block min-w-180 font-sans text-lg text-black font-normal pb-2 pr-3 border-b-2 border-black mb-8">Colors</div>

                <div class="flex flex-wrap w-332 mb-6">
                    @foreach($color_pallete as $block)
                        <div class="w-1/3 p-1">
                            <div class="font-medium text-sm text-{{ $block['text-color'] }} tracking-wide text-center p-2 h-24 bg-{{ $block['bg-color'] }}">{{ $block['color_hex'] }}</div>
                        </div>
                    @endforeach
                </div>
                <div class="w-332">
                    <a href="#" class="link mr-3">Link to URL</a>
                    <a href="#" class="link-hovered">Hover link</a>
                </div>
            </div>
            <div>
                <div class="inline-block min-w-180 font-sans text-lg text-black font-normal pb-2 pr-3 border-b-2 border-black mb-8">Logo</div>

                <div class="flex items-center">
                    <div class="mr-6">
                        <div class="logo-2"></div>
                    </div>
                    <div class="mr-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="115" height="115">
                            <path fill="#1B1B1B" fill-rule="evenodd" d="M114.788 62.424H77.156l2.658-5.425h35.18c.001.167.006.333.006.501a58.1 58.1 0 0 1-.212 4.924zM91.357 33.439c.425-.842-.106-1.301-1.169-1.301H79.559c-.85 0-1.594.306-1.806.918L63.935 62.672c-2.126 4.668-4.145 9.413-6.27 13.928h-.426c-1.913-4.438-4.038-9.413-6.164-13.928l-6.798-14.458-.095-.203-7.031-14.955c-.212-.612-.85-.918-1.7-.918H24.822c-1.169 0-1.594.459-1.275 1.301l7.139 14.572H.784C5.308 20.77 28.976 0 57.5 0s52.192 20.77 56.716 48.011H84.217l7.14-14.572zM37.748 62.424H.212A58.1 58.1 0 0 1 0 57.5c0-.168.005-.334.006-.501H35.09l2.658 5.425zm11.52 23.513c.425.612.956.918 1.807.918h12.86c.851 0 1.382-.306 1.701-.918l7.116-14.525h40.55C107.08 96.446 84.459 115 57.5 115S7.92 96.446 1.698 71.412h40.454l7.116 14.525z"/>
                        </svg>
                    </div>
                    <div class="w-171 h-157 bg-black flex justify-center items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="115" height="115">
                            <path fill="#FFF" fill-rule="evenodd" d="M114.788 62.424H77.156l2.658-5.425h35.18c.001.167.006.333.006.501a58.1 58.1 0 0 1-.212 4.924zM91.357 33.439c.425-.842-.106-1.301-1.169-1.301H79.559c-.85 0-1.594.306-1.806.918L63.935 62.672c-2.126 4.668-4.145 9.413-6.27 13.928h-.426c-1.913-4.438-4.038-9.413-6.164-13.928l-6.798-14.458-.095-.203-7.031-14.955c-.212-.612-.85-.918-1.7-.918H24.822c-1.169 0-1.594.459-1.275 1.301l7.139 14.572H.784C5.308 20.77 28.976 0 57.5 0s52.192 20.77 56.716 48.011H84.217l7.14-14.572zM37.748 62.424H.212A58.1 58.1 0 0 1 0 57.5c0-.168.005-.334.006-.501H35.09l2.658 5.425zm11.52 23.513c.425.612.956.918 1.807.918h12.86c.851 0 1.382-.306 1.701-.918l7.116-14.525h40.55C107.08 96.446 84.459 115 57.5 115S7.92 96.446 1.698 71.412h40.454l7.116 14.525z"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>

        <footer class="bg-orange-darkest text-lg text-grey p-8 ">FOOTER</footer>
    </div>

@endsection
