@extends("layouts.main")

@inject("localization_helper", "\App\Services\Localization\LocalizationHelper")

@section("content")
  <header class="main-header js-main-header--default">
    <div class="main-header__container">
      <!-- Hamburger icon -->
      <div class="main-header__container__nav-trigger js-nav-trigger"><span>@lang("navigation.main.Menu")</span></div>
      <div class="nav-desktop">
        <div class="logo-wrapper">
          <a href="{{ route('site.home') }}" class="logo">
            <span class="logo__brand-img"></span>
            <span class="logo__brand-name">Vincoin Cash</span>
          </a>
        </div>
        <div class="nav-sections">
          <div class="nav-secondary">
            <nav role="navigation" class="nav-wrapper">
              <ul class="nav">
                <li class="nav__item"><a class="nav__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
                @guest
                  <li class="nav__item"><a class="nav__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
                @endguest
                @auth
                  <li class="nav__item">
                    <a class="nav__link" href="javascript:void(0);">
                      <div class="flex items-center">
                        {{ auth()->user()->email  }} <span class="nav__link__arrow"></span>
                      </div>
                    </a>
                    <div class="nav__dropdown">
                      <div class="nav__dropdown__cover"></div>
                      <ul class="nav__dropdown__list">
                        <li class="nav__dropdown__item"><a href="{{ route("site.showAccount") }}" class="nav__link">@lang('navigation.main.Account')</a></li>
                        <li class="nav__dropdown__item">
                          <a href="{{ route('auth.logout') }}" class="nav__link" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">@lang('navigation.main.Log out')</a>
                          <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                          </form>
                        </li>
                      </ul>
                    </div>
                  </li>
                @endauth
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="language-wrapper">
        <div class="language-selector nav__item">
          <a class="nav__link" href="javascript:void(0);">
            <div class="flex items-center">
              {{ $localization_helper->getCurrLocale() }} <span class="nav__link__arrow"></span>
            </div>
          </a>
          <div class="nav__dropdown">
            <div class="nav__dropdown__cover"></div>
            <ul class="nav__dropdown__list js-select-language">
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl() }}" class="nav__link">EN</a></li>
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('ru') }}" class="nav__link">RU</a></li>
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('vn') }}" class="nav__link">VN</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="download-btn">
        <button class="btn" type="button"><a class="uppercase" href="{{ route("site.home") }}#section-wallet">@lang('navigation.main.Testnet is live')</a></button>
      </div>
    </div>
  </header>

  <div class="nav-mobile">
    <div class="nav-mobile__control nav-mobile__control--back btn-mobile--icon-back js-nav-trigger"><span>@lang("navigation.main.Back")</span></div>
    @guest
      <a href="{{ route('auth.showLogin') }}" class="nav-mobile__control nav-mobile__control--user btn-mobile--icon-user"><span>@lang('navigation.main.Sign In')</span></a>
    @endguest
    @auth
      <a href="{{ route('site.showAccount') }}" class="nav-mobile__control nav-mobile__control--user btn-mobile--icon-user"><span>@lang('navigation.main.Account')</span></a>
    @endauth
    <div class="logo-wrapper flex-no-shrink">
      <div class="inline-block">
        <a href="{{ route('site.home') }}" class="logo logo--white logo--col">
          <span class="logo__brand-img"></span>
          <span class="logo__brand-name">Vincoin Cash</span>
        </a>
      </div>
    </div>
    <div class="flex-shrink overflow-scroll">
      <!-- Navigation -->
      <nav id="ml-menu" role="navigation" class="nav-wrapper ml-menu">
        <div class="menu__wrap">
          <ul data-menu="main" class="nav nav--col menu__level">
            <li class="nav__item menu__item"><a class="nav__link menu__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
            @guest
              <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
            @endguest
            @auth
              <li class="nav__item menu__item">
                <a class="nav__link menu__link" data-submenu="submenu-2" href="javascript:void(0);">{{ auth()->user()->email  }}</a>
              </li>
            @endauth
          </ul>
          <ul data-menu="submenu-2" class="nav nav--col menu__level">
            <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ route("site.showAccount") }}">@lang('navigation.main.Account')</a></li>
            <li class="nav__item menu__item">
              <a class="nav__link menu__link" href="{{ route('auth.logout') }}" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">@lang('navigation.main.Log out')</a>
              <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
            <li class="nav__item menu__item"><a class="nav__link menu__link submenu-back uppercase" href="javascript:void(0);">
                <span class="back-arrow"></span>
                @lang('navigation.main.Back')
              </a></li>
          </ul>
        </div>
      </nav>
    </div>
    <div class="mt-2 mb-2 mx-auto"><button data-fancybox-close class="btn btn--transparent normal-case min-w-160 js-nav-trigger" type="button">@lang("navigation.main.Close Menu")</button></div>
  </div>

  <div class="section-smart-contract">
    <div class="section-smart-contract__container container">
      <h2 class="heading heading-md mb-6">@lang("Smart Contract")</h2>
      <p class="paragraph my-4 dont-break-out"><span class="font-bold">Token: </span>{{ config('admin.token-symbol') }} <br><span class="font-bold">Address: </span>{{ config('admin.smart-contract-address') }}</p>
      <p class="paragraph my-4 dont-break-out"><span class="font-bold">Etherscan: </span><a class="link" href="https://etherscan.io/token/{{ config('admin.smart-contract-address') }}" target="_blank">https://etherscan.io/token/{{ config('admin.smart-contract-address') }}</a></p>
    </div>
  </div>
@endsection
