@extends("layouts.main")

@push('after_styles')
  <link rel="stylesheet" href="{{ asset("/css/app.css") }}">
@endpush

@section("content")
<div class="page-not-found">
  <div class="page-not-found__mars"></div>
  <img src="https://mars-404.templateku.co/img/404.svg" class="page-not-found__logo-404" />
  <img src="https://mars-404.templateku.co/img/meteor.svg" class="page-not-found__meteor" />
  <p class="page-not-found__title">Oh no!!</p>
  <p class="page-not-found__subtitle">
    You’re either misspelling the URL <br /> or requesting a page that's no longer here.
  </p>
  <div class="text-center">
    <a class="page-not-found__btn-back" href="#">Back to previous page</a>
  </div>
  <img src="https://mars-404.templateku.co/img/astronaut.svg" class="page-not-found__astronaut" />
  <img src="https://mars-404.templateku.co/img/spaceship.svg" class="page-not-found__spaceship" />
</div>
@endsection