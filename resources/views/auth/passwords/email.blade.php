@extends('layouts.main')
{{--@inject("localization_helper", "\App\Services\Localization\LocalizationHelper")--}}

@section('content')
  <header class="main-header js-main-header--default">
      <div class="main-header__container">
          <!-- Hamburger icon -->
          <div class="main-header__container__nav-trigger js-nav-trigger"><span>@lang("navigation.main.Menu")</span></div>
          <div class="nav-desktop">
              <div class="logo-wrapper">
                  <a href="{{ route('site.home') }}" class="logo">
                      <span class="logo__brand-img"></span>
                      <span class="logo__brand-name">Vincoin Cash</span>
                  </a>
              </div>
              <div class="nav-sections">
                  <div class="nav-secondary">
                      <nav role="navigation" class="nav-wrapper">
                          <ul class="nav">
                              <li class="nav__item"><a class="nav__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
                              <li class="nav__item"><a class="nav__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
                          </ul>
                      </nav>
                  </div>
              </div>
          </div>
          {{--<div class="language-wrapper">--}}
              {{--<div class="language-selector nav__item">--}}
                  {{--<a class="nav__link" href="javascript:void(0);">--}}
                      {{--<div class="flex items-center">--}}
                          {{--{{ $localization_helper->getCurrLocale() }} <span class="nav__link__arrow"></span>--}}
                      {{--</div>--}}
                  {{--</a>--}}
                  {{--<div class="nav__dropdown">--}}
                      {{--<div class="nav__dropdown__cover"></div>--}}
                      {{--<ul class="nav__dropdown__list js-select-language">--}}
                          {{--<li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl() }}" class="nav__link">EN</a></li>--}}
                          {{--<li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('ru') }}" class="nav__link">RU</a></li>--}}
                          {{--<li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('vn') }}" class="nav__link">VN</a></li>--}}
                      {{--</ul>--}}
                  {{--</div>--}}
              {{--</div>--}}
          {{--</div>--}}
          <div class="download-btn">
              <button class="btn" type="button"><a class="" href="{{ route("site.home") }}#section-wallet">@lang('navigation.main.Download')</a></button>
          </div>
      </div>
  </header>

  <div class="nav-mobile">
      <div class="nav-mobile__control nav-mobile__control--back btn-mobile--icon-back js-nav-trigger"><span>@lang("navigation.main.Back")</span></div>
      @guest
          <a href="{{ route('auth.showLogin') }}" class="nav-mobile__control nav-mobile__control--user btn-mobile--icon-user"><span>@lang("navigation.main.Sign In")</span></a>
      @endguest
      <div class="logo-wrapper flex-no-shrink">
          <div class="inline-block">
              <a href="{{ route('site.home') }}" class="logo logo--white logo--col">
                  <span class="logo__brand-img"></span>
                  <span class="logo__brand-name">Vincoin Cash</span>
              </a>
          </div>
      </div>
      <div class="flex-shrink overflow-scroll">
          <!-- Navigation -->
          <nav id="ml-menu" role="navigation" class="nav-wrapper ml-menu">
              <div class="menu__wrap">
                  <ul data-menu="main" class="nav nav--col menu__level">
                      <li class="nav__item menu__item"><a class="nav__link menu__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
                      @guest
                          <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
                      @endguest
                  </ul>
              </div>
          </nav>
      </div>
      <div class="mt-2 mb-2 mx-auto"><button data-fancybox-close class="btn btn--transparent normal-case min-w-160 js-nav-trigger" type="button">@lang("navigation.main.Close Menu")</button></div>
  </div>

<div class="section-reset-pass">
    <div class="section-reset-pass__container container">
        <div class="logo-big section-logo"></div>
        <div class="text-center mb-8">
            <h2 class="heading heading-md">{{ __('Reset Password') }}</h2>
        </div>
        <div class="vin-form-wrapper">
            @if (session('status'))
                <div class="alert alert--default" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ route('password.email') }}" class="vin-form vin-form--transparent reset-pass-form" method="POST">
                @csrf
                <div class="form__holder">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="form-label">
                            @lang("E-mail")
                        </label>
                        <input type="text" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="your@mail.com" required autofocus>
                        @if ($errors->has('email'))
                            <div class="help-block" data-for="email"><strong>{{ $errors->first('email') }}</strong></div>
                        @endif
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn--red submit-btn ladda-button mx-auto mt-8 mb-8" type="submit" id="pass-reset-submit" data-style="zoom-in"><span class="ladda-label">{{ __('Send Password Reset Link') }}</span></button>
                    </div>
                </div>
            </form>
        </div>

        {{--<div class="text-center">--}}
            {{--<div class="cards cards--red">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-header">{{ __('Reset Password') }}</div>--}}

                    {{--<div class="card-body">--}}
                        {{--@if (session('status'))--}}
                            {{--<div class="alert alert-success" role="alert">--}}
                                {{--{{ session('status') }}--}}
                            {{--</div>--}}
                        {{--@endif--}}

                        {{--<form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">--}}
                            {{--@csrf--}}

                            {{--<div class="form-group row">--}}
                                {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

                                    {{--@if ($errors->has('email'))--}}
                                        {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group row mb-0">--}}
                                {{--<div class="col-md-6 offset-md-4">--}}
                                    {{--<button type="submit" class="btn btn-primary">--}}
                                        {{--{{ __('Send Password Reset Link') }}--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
@endsection
