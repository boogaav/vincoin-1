<?php

return [
    'main' => [
        'Navigation' => 'Навигация',
        'Menu' => 'Меню',
        'Close Menu' => 'Закрыть Меню',
        'Back' => 'Назад',
        'Get started' => 'Начать',
        'Mission' => 'Миссия',
        'Road map' => 'План проекта',
        'About us' => 'О нас',
        'Services' => 'Услуги',
        'Buy Vincoin Cash' => 'Купить Vincoin Cash',
        'Trade Vincoin Cash' => 'Обмен Vincoin Cash',
        'Mine Vincoin Cash' => 'Добыча Vincoin Cash',
        'White Paper' => 'White Paper',
        'Block explorer' => 'Block explorer',
        'Faq' => 'Faq',
        'Contact Us' => 'Контакты',
        'Sign In/Sign Up' => 'Вход/Регистрация',
        'Sign In' => 'Войти',
        'Account' => 'Кабинет',
        'Log out' => 'Выход',
        'Download' => 'Скачать',
        'Write to support' => 'Поддержка',
        'Online chat' => 'Онлайн чат',
        'Information' => 'Информация',
        'Testnet is live' => 'Testnet is live'
    ]
];
